
<?php
function ubah_huruf($string){
    $arr1 = str_split($string);
    $length = count($arr1);
    for ($i = 0; $i < $length; $i++) {
        $arr2[$i] = ++$arr1[$i];
    }
    echo implode($arr2)."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>